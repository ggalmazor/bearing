package com.github.ggalmazor.bearing;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import es.blackleg.java.geocalc.DegreeCoordinate;
import es.blackleg.java.geocalc.EarthCalc;
import es.blackleg.java.geocalc.Point;
import org.slf4j.bridge.SLF4JBridgeHandler;

public class Main {
  private static final GeoApiContext CONTEXT = new GeoApiContext().setApiKey("AIzaSyCyz5JmutPfIQF8Z-BwQ8o2kGx7fqgD5Xo");

  public static void main(String[] args) throws Exception {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();

    GeocodingResult[] homeGeocoding = GeocodingApi.geocode(CONTEXT, args[0]).await();
    Point home = new Point(
        new DegreeCoordinate(homeGeocoding[0].geometry.location.lat),
        new DegreeCoordinate(homeGeocoding[0].geometry.location.lng)
    );

    GeocodingResult[] targetGeocoding = GeocodingApi.geocode(CONTEXT, args[1]).await();
    Point target = new Point(
        new DegreeCoordinate(targetGeocoding[0].geometry.location.lat),
        new DegreeCoordinate(targetGeocoding[0].geometry.location.lng)
    );

    double bearing = EarthCalc.getBearing(home, target);
    System.out.println("Home: " + args[0] + " >> " + home.getLatitude() + ", " + home.getLongitude());
    System.out.println("Target: " + args[1] + " >> " + target.getLatitude() + ", " + target.getLongitude());
    System.out.println("Azimuth: " + bearing);
  }

}
