export default class Point {
  constructor(x, y, z, radius, nx, ny, nz) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.radius = radius;
    this.nx = nx;
    this.ny = ny;
    this.nz = nz;
  }

  static normalizeVectorDiff(a, b) {
    if (!(a instanceof Point) || !(b instanceof Point))
      throw new Error("Both a and b must be Point instances");

    // calculate norm(b-a), where norm divides a vector by its length to produce a unit vector.
    const dx = b.x - a.x;
    const dy = b.y - a.y;
    const dz = b.z - a.z;
    const distSquared = dx * dx + dy * dy + dz * dz;
    return distSquared == 0
        ? null
        : {
          x: (dx / Math.sqrt(distSquared)),
          y: (dy / Math.sqrt(distSquared)),
          z: (dz / Math.sqrt(distSquared)),
          radius: 1.0
        };
  }

  distanceTo(other) {
    if (!(other instanceof Point))
      throw new Error("Other must be a Point");
    const dx = this.x - other.x;
    const dy = this.y - other.y;
    const dz = this.z - other.z;
    return Math.sqrt(dx * dx + dy * dy + dz * dz);
  }
}