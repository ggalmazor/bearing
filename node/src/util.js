import Location from './location.js';

export const equatorialRadius = 6378137.0;
export const polarRadius = 6356752.3;
export const eccentricitySquared = 0.00669437999014;

export const toRads = degrees => degrees * Math.PI / 180.0;

// http://en.wikipedia.org/wiki/Earth_radius
export const earthRadius = latitudeRadians => {
  const t1 = Math.pow(equatorialRadius, 2) * Math.cos(latitudeRadians);
  const t2 = Math.pow(polarRadius, 2) * Math.sin(latitudeRadians);
  const t3 = equatorialRadius * Math.cos(latitudeRadians);
  const t4 = polarRadius * Math.sin(latitudeRadians);
  return Math.sqrt((t1 * t1 + t2 * t2) / (t3 * t3 + t4 * t4));
};

// Convert geodetic latitude 'lat' to a geocentric latitude 'clat'.
// Geodetic latitude is the latitude as given by GPS.
// Geocentric latitude is the angle measured from center of Earth between a point and the equator.
// https://en.wikipedia.org/wiki/Latitude#Geocentric_latitude
export const geodeticToGeocentricLatitude = lat => Math.atan((1.0 - eccentricitySquared) * Math.tan(lat));

export const bindToCircunference = theta => {
  if (theta < 0.0)
    return theta + 360.0;
  if (theta > 360.0)
    return theta - 360.0;
  return theta;
};

export const rotateGlobe = (b, a, radiusAtB) => {
  // Get modified coordinates of 'b' by rotating the globe so that 'a' is at lat=0, lon=0.
  const br = new Location(b.lat, b.lon - a.lon, b.elv);
  const brp = br.toPoint();

  // Rotate brp cartesian coordinates around the z-axis by a.lon degrees,
  // then around the y-axis by a.lat degrees.
  // Though we are decreasing by a.lat degrees, as seen above the y-axis,
  // this is a positive (counterclockwise) rotation (if B's longitude is east of A's).
  // However, from this point of view the x-axis is pointing left.
  // So we will look the other way making the x-axis pointing right, the z-axis
  // pointing up, and the rotation treated as negative.

  const alat = geodeticToGeocentricLatitude(toRads(-a.lat));
  const bx = (brp.x * Math.cos(alat)) - (brp.z * Math.sin(alat));
  const by = brp.y;
  const bz = (brp.x * Math.sin(alat)) + (brp.z * Math.cos(alat));

  return {
    x: bx,
    y: by,
    z: bz,
    radius: radiusAtB
  };
};