import {rotateGlobe, bindToCircunference} from "./util.js";
import Point from "./point.js";
import Location from "./location.js";
var NodeGeocoder = require('node-geocoder');

var options = {
  provider: 'google',

  httpAdapter: 'https',
  apiKey: 'AIzaSyCyz5JmutPfIQF8Z-BwQ8o2kGx7fqgD5Xo',
  formatter: null         // 'gpx', 'string', ...
};

var geocoder = NodeGeocoder(options);

function calculate(locationA, locationB) {
  const pointA = locationA.toPoint();
  const pointB = locationB.toPoint();

  console.log(`Distance: ${pointA.distanceTo(pointB)} m`);

  // Let's use a trick to calculate azimuth:
  // Rotate the globe so that point A looks like latitude 0, longitude 0.
  // We keep the actual radii calculated based on the oblate geoid,
  // but use angles based on subtraction.
  // Point A will be at x=radius, y=0, z=0.
  // Vector difference B-A will have dz = N/S component, dy = E/W component.
  const {y, z} = rotateGlobe(locationB, locationA, pointB.radius);
  if (z * z + y * y > 1.0e-6) {
    const theta = 90.0 - Math.atan2(z, y) * 180.0 / Math.PI;
    const azimuth = bindToCircunference(theta);
    console.log(`Azimuth: ${azimuth}`);
  }

  const bma = Point.normalizeVectorDiff(pointB, pointA);
  if (bma != null) {
    // calculate altitude, which is the angle above the horizon of B as seen from A.
    // Almost always, B will actually be below the horizon, so the altitude will be negative.
    // The dot product of bma and norm = cos(zenith_angle), and zenith_angle = (90 deg) - altitude.
    // So altitude = 90 - acos(dotprod).
    const altitude = 90.0 - (180.0 / Math.PI) * Math.acos(bma.x * pointA.nx + bma.y * pointA.ny + bma.z * pointA.nz);
    console.log(`Altitude: ${altitude}`);
  }
}


const home = new Location(43.3056057, -1.9812582, 0);

geocoder.geocode(process.argv[2])
    .then(([{latitude, longitude}]) => {
      console.log(`Target: ${latitude}, ${longitude}`);
      console.log(`http://www.google.com/maps/place/${latitude},${longitude}`);
      const target = new Location(latitude, longitude, 0);
      calculate(home, target);
    })
    .catch(function(err) {
      console.log(err);
    });


