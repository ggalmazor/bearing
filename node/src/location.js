import {earthRadius, toRads, geodeticToGeocentricLatitude} from "./util.js";
import Point from "./point.js";

export default class Location {
  constructor(lat, lon, elv) {
    this.lat = lat;
    this.lon = lon;
    this.elv = elv;
  }

  toPoint() {
    const radius = earthRadius(toRads(this.lat));
    const geocentricLat = geodeticToGeocentricLatitude(toRads(this.lat));

    // Get x, y, z
    const flatX = radius * Math.cos(toRads(this.lon)) * Math.cos(geocentricLat);
    const flatY = radius * Math.sin(toRads(this.lon)) * Math.cos(geocentricLat);
    const flatZ = radius * Math.sin(geocentricLat);

    // We used geocentric latitude to calculate (x,y,z) on the Earth's ellipsoid.
    // Now we use geodetic latitude to calculate normal vector from the surface, to correct for elevation.
    const nx = Math.cos(toRads(this.lat)) * Math.cos(toRads(this.lon));
    const ny = Math.cos(toRads(this.lat)) * Math.sin(toRads(this.lon));
    const nz = Math.sin(toRads(this.lat));
    const x = flatX + this.elv * nx;
    const y = flatY + this.elv * ny;
    const z = flatZ + this.elv * nz;

    return new Point(x, y, z, radius, nx, ny, nz);
  }

}
